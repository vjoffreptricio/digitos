import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.*;

public class Digitos {

    private static <T> T[] append(T[] arr, T element) { //Funcion para formar array de array´s
        T[] array = Arrays.copyOf(arr, arr.length + 1);
        array[arr.length] = element;
        return array;
    }

    public static void main(String[] args) throws FileNotFoundException {


        String numero = "2664327"; //Numero solicitado
        
        String[][] texto = {};
        String[] uno = {"A", "B", "C"};
        String[] dos = {"D", "E", "F"};
        String[] tres = {"G", "H", "I"};
        String[] cuatro = {"J", "K", "L"};
        String[] cinco = {"M", "N", "O"};
        String[] seis = {"P", "Q", "R", "S"};
        String[] siete = {"T", "U", "V"};
        String[] ocho = {"W", "X", "Y"};
        
        if (numero.length() == 7) {
            for (int n = 0; n < numero.length(); n++) {
                char c = numero.charAt(n);
                if (Character.toString(c).equals("2")) {
                    texto = append(texto, uno);
                } else if (Character.toString(c).equals("3")) {
                    texto = append(texto, dos);
                } else if (Character.toString(c).equals("4")) {
                    texto = append(texto, tres);
                } else if (Character.toString(c).equals("5")) {
                    texto = append(texto, cuatro);
                } else if (Character.toString(c).equals("6")) {
                    texto = append(texto, cinco);
                } else if (Character.toString(c).equals("7")) {
                    texto = append(texto, seis);
                } else if (Character.toString(c).equals("8")) {
                    texto = append(texto, siete);
                } else if (Character.toString(c).equals("9")) {
                    texto = append(texto, ocho);
                }
            }
        }

        try (PrintStream textoR = new PrintStream("C:/Users/jpverdezoto/palabras.txt")) {
            for (int a = 0; a < texto[0].length; a++) {
                for (int b = 0; b < texto[1].length; b++) {
                    for (int c = 0; c < texto[2].length; c++) {
                        for (int d = 0; d < texto[3].length; d++) {
                            for (int e = 0; e < texto[4].length; e++) {
                                for (int f = 0; f < texto[5].length; f++) {
                                    for (int g = 0; g < texto[6].length; g++) {
                                        textoR.println( //guardamos el resultado en un archivo txt
                                                texto[0][a] + texto[1][b] + texto[2][c] + texto[3][d] + texto[4][e] + texto[5][f] + texto[6][g]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            textoR.close();
			System.out.println("Archivo generado satisfactoriamente para el NUMERO: "+ numero);
			System.out.println("UBICACION DEL ARCHIVO: C:/Users/jpverdezoto/palabras.txt");
        }
    }
}